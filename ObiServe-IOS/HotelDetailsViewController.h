//
//  HotelDetailsViewController.h
//  ObiServe
//
//  Created by Alpaslan Bak on 1.06.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelDetailsViewController : UIViewController{


}
- (IBAction)backButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property(nonatomic) NSString *htmlCode;
@end
