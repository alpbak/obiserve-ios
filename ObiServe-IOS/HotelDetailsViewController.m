//
//  HotelDetailsViewController.m
//  ObiServe
//
//  Created by Alpaslan Bak on 1.06.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import "HotelDetailsViewController.h"
#import "HotelMainViewController.h"

@interface HotelDetailsViewController ()

@end

@implementation HotelDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //NSLog(@"WHAT: %@", self.htmlCode);
    
    [self.webView loadHTMLString:self.htmlCode baseURL:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonAction:(id)sender {
    HotelMainViewController *viewController = [[HotelMainViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];

}
@end
