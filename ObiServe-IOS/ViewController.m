//
//  ViewController.m
//  ObiServe-IOS
//
//  Created by Alpaslan Bak on 27.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import "ViewController.h"
#import "ChoiceViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"Main");
    ChoiceViewController *viewController = [[ChoiceViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
    


    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
