//
//  ServiceRequestViewController.h
//  ObiServe
//
//  Created by Alpaslan Bak on 1.06.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ServiceRequestViewController : UIViewController<CLLocationManagerDelegate>{
    NSString *serviceCode;
    NSString *serviceNote;
    CLLocationManager *locationManager;
    float currentLocationLat;
    float currentLocationLon;
    float hotelLocationLat;
    float hotelLocationLon;
    float hotelradius;
    BOOL locationKnown;
}
@property (strong, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *technicView;
@property (strong, nonatomic) IBOutlet UIView *wakeUpView;
@property (strong, nonatomic) IBOutlet UIView *houseKeepingView;
@property (strong, nonatomic) IBOutlet UILabel *houseKeepingLabel;
@property (strong, nonatomic) IBOutlet UILabel *wakeUpLabel;
@property (strong, nonatomic) IBOutlet UILabel *tecnicLabel;

@property (strong, nonatomic) IBOutlet UIView *waitView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *waitIndicator;
@property (strong, nonatomic) IBOutlet UILabel *pleaseWaitLabel;
@property (strong, nonatomic) IBOutlet UIButton *waitCloseButton;
- (IBAction)waitCloseButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *requestText;
@property (strong, nonatomic) IBOutlet UILabel *requestLabel;
- (IBAction)requestSendButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *requestSendButton;
- (IBAction)closeButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *requestEntryView;
@property (strong, nonatomic) IBOutlet UIView *setAlarmView;
@property (strong, nonatomic) IBOutlet UILabel *wakeUpTimeLabel;
- (IBAction)alarmCloseAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)datePickerAction:(id)sender;
- (IBAction)alarmRequestSendButtonAction:(id)sender;


@end
