//
//  ChoiceViewController.m
//  ObiServe-IOS
//
//  Created by Alpaslan Bak on 27.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import "ChoiceViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "HotelMainViewController.h"

@interface ChoiceViewController ()

@end

@implementation ChoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"ChoiceViewController");
    
    self.loginButton.layer.cornerRadius = 20;
    self.loginButton.clipsToBounds = YES;
    self.loginButton.layer.borderWidth = 1;
    self.loginButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    [self.loginButton setTitle:NSLocalizedString(@"LOG IN", @"LOG IN") forState:UIControlStateNormal];
    [self.signUpButton setTitle:NSLocalizedString(@"SIGN UP", @"SIGN UP") forState:UIControlStateNormal];
    
    
    
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]stringForKey:@"apiKey"];
    BOOL isQRAvailable =[[NSUserDefaults standardUserDefaults]boolForKey:@"isQRAvailable"];
    
    if ([apiKey length] > 0 && isQRAvailable) {
        [self performSelector:@selector(goToScanner) withObject:self afterDelay:1.2 ];
    }
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)loginButtonAction:(UIButton *)sender {
    NSLog(@"Login Click");
    LoginViewController *viewController = [[LoginViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)registerButtonAction:(UIButton *)sender {
    NSLog(@"Register Click");
    RegisterViewController *viewController = [[RegisterViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}

-(void)goToScanner{
    HotelMainViewController *viewController = [[HotelMainViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}


@end
