//
//  MenuListViewController.h
//  ObiServe-IOS
//
//  Created by Alpaslan Bak on 27.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MenuListViewController : UIViewController<CLLocationManagerDelegate>{
    NSString *qrCode;
    NSString *deviceLanguage;
    NSArray *mainMenuArray;
    NSMutableArray *yemekArray;
    NSMutableArray *tempBasketArray;
    NSMutableArray *basketArray;
    NSMutableArray *categoryNameArray;
    int mainBasketCount;
    NSString *orderString;
    CLLocationManager *locationManager;
    float currentLocationLat;
    float currentLocationLon;
    float hotelLocationLat;
    float hotelLocationLon;
    float hotelradius;
    
    BOOL locationKnown;
    
    NSString *serviceStartTime;
    NSString *serviceEndTime;
    
    BOOL shouldCheckOrderTime;
}
- (IBAction)backButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *buttonView;
@property (strong, nonatomic) IBOutlet UILabel *orderLabel;
@property (strong, nonatomic) IBOutlet UILabel *basketQuantitiyLabel;
@property (strong, nonatomic) IBOutlet UIView *waitView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *waitIndicator;
@property (strong, nonatomic) IBOutlet UILabel *pleaseWaitLabel;
@property (strong, nonatomic) IBOutlet UIButton *waitCloseButton;
- (IBAction)waitCloseButtonAction:(id)sender;

@end
