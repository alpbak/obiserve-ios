//
//  MenuListViewController.m
//  ObiServe-IOS
//
//  Created by Alpaslan Bak on 27.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import "MenuListViewController.h"
#import "AFNetworking.h"
#import "Constant.pch"
#import "SDWebImageDownloader.h"
#import "HomeCustomCell.h"
#import "HotelMainViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MenuListViewController ()

@end

@implementation MenuListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    
    
    
    qrCode = [[NSUserDefaults standardUserDefaults] stringForKey:@"qrCode"];
    deviceLanguage = [[NSUserDefaults standardUserDefaults] stringForKey:@"deviceLanguage"];
    
    self.buttonView.layer.cornerRadius = 25;
    self.buttonView.clipsToBounds = YES;
    
    self.orderLabel.text = NSLocalizedString(@"ORDER", @"ORDER");
    self.pleaseWaitLabel.text = NSLocalizedString(@"PLEASE WAIT", @"PLEASE WAIT");
    [self.waitCloseButton setTitle:NSLocalizedString(@"TAP HERE TO CLOSE", @"TAP HERE TO CLOSE") forState:UIControlStateNormal];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleOrderTap:)];
    [self.buttonView addGestureRecognizer:singleFingerTap];
    
    mainBasketCount = 0;
    
    [self checkUserActive];
}


-(void)viewDidDisappear:(BOOL)animated
{
    [locationManager stopUpdatingLocation];
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    locationKnown = NO;
    
    
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:NSLocalizedString(@"Error", @"Error")
                               message:NSLocalizedString(@"Failed to Get Your Location. Please enable location services in order to send an order.", @"Failed to Get Your Location. Please enable location services in order to send an order")
                               delegate:nil
                               cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                               otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        //longitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        //latitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        //NSLog(@"LATITUDE: %.8f", currentLocation.coordinate.latitude);
        //NSLog(@"LONGITUDE: %.8f", currentLocation.coordinate.longitude);
        
        locationKnown = YES;
        
        currentLocationLat = currentLocation.coordinate.latitude;
        currentLocationLon = currentLocation.coordinate.longitude;
        
    }
}

- (void)handleOrderTap:(UITapGestureRecognizer *)recognizer {
    
    if (locationKnown) {
        if ([self isUserCloseToOrder]) {
            
            if ([self checkOrderTimeCorrect]) {
                [self prepareOrder];
            }
            else{
                [self showHoursNotAvailableAlert];
            }
            
        }
        else{
            [self showNotCloseAlert];
        }
    }
    else{
        [self showLocationNotAvailableAlert];
    }


    
}

-(Boolean) checkOrderTimeCorrect{
    if (shouldCheckOrderTime) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"HH:mm:ss"];
        [dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"Europe/Istanbul"]];
        NSDate *currentTime = [NSDate date];
        NSString *currentTimeString = [dateFormat stringFromDate: currentTime];
        NSLog(@"currentTime: String %@", currentTimeString);
        
        Boolean startTime = [self checkOrderTime:currentTimeString :serviceStartTime];
        Boolean endTime = [self checkOrderTime:currentTimeString :serviceEndTime];
        
        NSLog(@"CHECK START TIME: %d", [self checkOrderTime:currentTimeString :serviceStartTime]);
        NSLog(@"CHECK END TIME: %d", [self checkOrderTime:currentTimeString :serviceEndTime]);
        
        if (startTime && !endTime) {
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return true;
    }
    
}

-(void)showNotCloseAlert{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message:NSLocalizedString(@"You are not close enough to the hotel. Please get closer and try again", @"You are not close enough to the hotel. Please get closer and try again")
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}

-(void)showHoursNotAvailableAlert{
    NSString *msg = NSLocalizedString(@"You can't order at this time. Please note that the room service is available between: ", @"You can't order at this time. Please note that the room service is available between: ");
    NSString *combinedMessage = [NSString stringWithFormat:@"%@ %@ - %@", msg, serviceStartTime, serviceEndTime];

    
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message: combinedMessage
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}
-(void)showLocationNotAvailableAlert{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message:NSLocalizedString(@"We can not get your location. Please activate location services and try again.", @"We can get your location. Please activate location services and try again.")
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}

-(void)prepareOrder{
    int existingQty = [self.basketQuantitiyLabel.text intValue];
    
    if (existingQty > 0) {
        
        orderString=@"";
        
        for (id dish in basketArray) {
            NSString *dishId = [dish valueForKey:@"dishId"];
            NSString *quantity = [dish valueForKey:@"dishQuantitiy"];
            
            //NSLog(@"dishId: %@", dishId);
            //NSLog(@"quantity: %@", quantity);
            
            if (![quantity isEqualToString:@"0"]) {
                orderString = [NSString stringWithFormat:@"%@%@-%@,", orderString, dishId,quantity];
            }
        }
        
        orderString = [orderString substringToIndex:[orderString length]-1];
        NSLog(@"orderString: %@", orderString);
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"Send Order?", @"Send Order?")
                              message:NSLocalizedString(@"You are sending this order. Are you sure?", @"You are sending this order. Are you sure?")
                              delegate:self
                              cancelButtonTitle:NSLocalizedString(@"No", @"No")
                              otherButtonTitles:NSLocalizedString(@"Yes", @"Yes"),
                              nil];
        [alert show];
        
    }
}

-(BOOL)isUserCloseToOrder{
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:currentLocationLat longitude:currentLocationLon];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:hotelLocationLat longitude:hotelLocationLon];
    float distance = [location1 distanceFromLocation:location2];
    
    NSLog(@"Distance in meters: %f", distance);
    
    NSLog(@"Hotel radius in meters: %f", hotelradius);
    
    if (hotelradius >= distance) {
        return true;
    }
    else{
        return false;
    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self doOrder];
    }
}

-(void)getMenu{
    yemekArray = [[NSMutableArray alloc] init];
    categoryNameArray = [[NSMutableArray alloc] init];
    tempBasketArray = [[NSMutableArray alloc] init];
    basketArray = [[NSMutableArray alloc] init];

    
    //NSLog(@"MenuListView -- getMenu");
    
    //[self.activityIndicatorView setHidden:false];
    
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:qrCode forKey:@"qrcode"];
    [dict setValue:deviceLanguage forKey:@"requestLanguage"];
    [dict setValue:@"40.998" forKey:@"requestLatitude"];
    [dict setValue:@"26.998" forKey:@"requestLongitude"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:GETMENUS_API];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 //NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     NSArray *hotelDetail = [dictRes valueForKey:@"hotelDetails"];
                                                     NSArray *xxx = [hotelDetail objectAtIndex:0];
                                                     
                                                     hotelLocationLat = [[xxx valueForKey:@"hotel_lat"] floatValue];
                                                     hotelLocationLon = [[xxx valueForKey:@"hotel_lon"] floatValue];
                                                     hotelradius = [[xxx valueForKey:@"order_radius"] floatValue];
                                                     
                                                     
                                                     serviceStartTime = [xxx valueForKey:@"hotel_service_start"];
                                                     serviceEndTime = [xxx valueForKey:@"hotel_service_end"];
                                                     
                                                     NSLog(@"serviceStart: %@", serviceStartTime);
                                                     NSLog(@"serviceEnd: %@", serviceEndTime);
                                                     
                                                     if (!serviceStartTime
                                                         || [serviceStartTime isKindOfClass:[NSNull class]]
                                                         || !serviceEndTime
                                                         || [serviceEndTime isKindOfClass:[NSNull class]]
                                                         || [serviceStartTime isEqualToString:@"00:00:00"]
                                                         || [serviceStartTime isEqualToString:@"00:00:00"]){
                                                         //NSLog(@"START IS EMPTY");
                                                         shouldCheckOrderTime = NO;
                                                     }
                                                     else{
                                                         shouldCheckOrderTime = YES;
                                                     }
                                                         
                                                         
                                                     
                                                     mainMenuArray = [dictRes valueForKey:@"menus"];
                                                     
                                                     
                                                     for (int i=0; i< [mainMenuArray count]; i++){
                                                         
                                                         
                                                         NSArray *categoryMain = [mainMenuArray[i] valueForKey:@"category"];
                                                         NSArray *categoryDetails = [categoryMain valueForKey:@"catArray"];
                                                         NSString *categoryName = [categoryMain valueForKey:@"category_name"];
                                                         
                                                         [categoryNameArray addObject:categoryName];
                                                         [yemekArray addObject:categoryDetails];
                                                         
                                                         
                                                         
                                                     }
                                                     
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             //NSLog(@"basketArray %@", basketArray);
                                             //NSLog(@"yemekArray %@", yemekArray);
                                             //NSLog(@"categoryNameArray %@", categoryNameArray);
                                             
                                             [self.tableView reloadData];
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                             
                                             
                                             
                                             
                                             
                                             
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
}

-(Boolean) checkOrderTime:(NSString *)currentTime :(NSString*)orderTime{
    
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    NSDate *currentDate= [formatter dateFromString:currentTime];
    NSDate *orderDate = [formatter dateFromString:orderTime];
    
    NSComparisonResult result = [currentDate compare:orderDate];
    if(result == NSOrderedDescending)
    {
        NSLog(@"currentDate is later than orderDate");
        return true;
    }
    else if(result == NSOrderedAscending)
    {
        NSLog(@"orderDate is later than currentDate");
        return false;
    }
    else
    {
        NSLog(@"currentDate is equal to orderDate");
        return false;
    }
    
}


-(void)showAlert: (NSString *) message{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message:message
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //NSLog(@"view will appear");
    self.tableView.estimatedRowHeight = 220.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [categoryNameArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Number of rows is the number of time zones in the region for the specified section.
    
    return [[yemekArray objectAtIndex:section] count];
    

    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *rowData;
    NSArray *mainRowData;
    
    //NSLog(@"cellForRowAtIndexPath - indexPath %ld", (long)indexPath.row);


    
    @try {
        mainRowData = [yemekArray objectAtIndex:indexPath.section];
        //NSLog(@"mainRowData: %@", mainRowData);
        
        rowData = [mainRowData objectAtIndex:indexPath.row];
        //NSLog(@"ROWDATA: %@", rowData);

        
    } @catch (NSException *exception) {
        [self.tableView reloadData];
    }
    
    //NSLog(@"ROWDATA: %@", rowData);
    
    
    
    static NSString *CellIdentifier = @"TourCell";
    HomeCustomCell *cell=[self.tableView  dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeCustomCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    //cell.mainView.layer.borderWidth = 1;
    //cell.mainView.layer.cornerRadius = 10;
    //cell.mainView.layer.borderColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1].CGColor; /*#cccccc*/
    //cell.mainView.layer.masksToBounds = true;
    
    
    
    
    //emir = [emir stringByReplacingOccurrencesOfString: @ "&nbsp;" withString: @ " "];
    
    
    NSString *dishName = [rowData valueForKey:@"dish_name"];
    cell.name.text = dishName;
    
    NSInteger price = [[rowData valueForKey:@"dish_price"] integerValue];
    NSString *priceStr = [NSString stringWithFormat:@"%ld",(long)price];
    NSString *currency = [rowData valueForKey:@"dish_price_currency"];
    NSString *combinedPrice = [NSString stringWithFormat:@"%@ %@", priceStr, currency];
    cell.price.text = combinedPrice;
    
    NSString *desc;
    if ([rowData valueForKey:@"dish_description"] != [NSNull null]) {
        desc = [rowData valueForKey:@"dish_description"];
    }
    else{
        desc = @"";
    }
    
    
  
    
    cell.desc.text = desc;
    
    NSString *dishImage = [rowData valueForKey:@"image"];
    NSString *combined = [NSString stringWithFormat:@"%@%@", @"http://www.obiservis.com/images/", dishImage];
    
    [cell.dishImage sd_setImageWithURL:[NSURL URLWithString:combined]
                           placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    
     
    //BUTTONS
    [cell.plusButton addTarget:self action:@selector(plusClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.minusButton addTarget:self action:@selector(minusClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString *selectedIndexId = [NSString stringWithFormat:@"%ld", indexPath.section];
    cell.plusButton.titleLabel.text = selectedIndexId;
    cell.plusButton.titleLabel.hidden = YES;
    
    cell.minusButton.titleLabel.text = selectedIndexId;
    cell.minusButton.titleLabel.hidden = YES;
    
    NSString *dishId = [rowData valueForKey:@"id"];
    
    for (id dish in basketArray) {
        NSString *searchDishId = [dish valueForKey:@"dishId"];
        
        if ([searchDishId intValue] == [dishId intValue]) {
            cell.quantitiyLabel.text = [dish valueForKey:@"dishQuantitiy"];
        }
    }
    
    return cell;
}

-(void)plusClicked:(UIButton *)sender{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil)
    {
        NSInteger currentIndex = indexPath.row;
        NSInteger tableSection = indexPath.section;
        
        NSIndexPath *indexPathNew = [NSIndexPath indexPathForRow:currentIndex inSection:tableSection];
        
        HomeCustomCell *cell = [self.tableView cellForRowAtIndexPath:indexPathNew];
        int existingQty = [cell.quantitiyLabel.text intValue];
        
        if (existingQty >= 0) {
            existingQty = existingQty + 1;
            cell.quantitiyLabel.text = [NSString stringWithFormat:@"%d", existingQty];
            
            [self updateMainBasketCount:1];
            
            [self addToBasket:tableSection :currentIndex];
        }
    }
}

-(void)minusClicked:(UIButton *)sender{

    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil)
    {
        NSInteger currentIndex = indexPath.row;
        NSInteger tableSection = indexPath.section;
        
        NSIndexPath *indexPathNew = [NSIndexPath indexPathForRow:currentIndex inSection:tableSection];
        
        HomeCustomCell *cell = [self.tableView cellForRowAtIndexPath:indexPathNew];
        
        int existingQty = [cell.quantitiyLabel.text intValue];
        
        
        if (existingQty > 0) {
            existingQty = existingQty - 1;
            cell.quantitiyLabel.text = [NSString stringWithFormat:@"%d", existingQty];
            
            [self updateMainBasketCount:-1];
            
            [self removeFromBasket:tableSection :currentIndex];
        }

    }
}

-(void)addToBasket: (NSInteger)selectedSection :(NSInteger) selectedRow{
    NSArray *selectedMainRowData;
    NSArray *selectedRowData;
    
    selectedMainRowData = [yemekArray objectAtIndex:selectedSection];
    selectedRowData = [selectedMainRowData objectAtIndex:selectedRow];
    
    NSString *dishId = [NSString stringWithFormat:@"%@",[selectedRowData valueForKey:@"id"]];
    //NSLog(@"Selected DISH ID: %@", dishId);
    //NSLog(@"SEPETTEMI: %d", [self itemExists:@"dishId" withValue:dishId]);
    
    if (![self itemExists:@"dishId" withValue:dishId]) {
        //NSLog(@"NOT IN BASKET Let's add this");
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        [dictionary setObject:dishId forKey:@"dishId"];
        [dictionary setObject:@"1" forKey:@"dishQuantitiy"];
        
        [basketArray addObject:dictionary];
    }
    else{
        //NSLog(@"IN BASKET Let's NOT add this.. But update the quantity");
        
        NSUInteger arrayIndex = [basketArray indexOfObjectPassingTest:^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop){
            return [[dict objectForKey:@"dishId"] isEqual:dishId];
        }];
        
        NSString *currentQuantityStr = [[basketArray objectAtIndex:arrayIndex] valueForKey:@"dishQuantitiy"];
        NSInteger currentQuantity = [currentQuantityStr integerValue];
        NSInteger newQuantity = currentQuantity + 1;
        NSString *newQuantitiyStr = [NSString stringWithFormat:@"%ld", (long)newQuantity];
        
        //NSLog(@"IN BASKET INDEX: %lu", (unsigned long)arrayIndex);
        
        NSMutableDictionary *dictionary2 = [NSMutableDictionary dictionary];
        [dictionary2 setObject:dishId forKey:@"dishId"];
        [dictionary2 setObject:newQuantitiyStr forKey:@"dishQuantitiy"];
        
        [basketArray replaceObjectAtIndex:arrayIndex withObject:dictionary2];
    }

    
    //NSLog(@"BASKET ARRAY: %@", basketArray);
}

-(void)removeFromBasket: (NSInteger)selectedSection :(NSInteger) selectedRow{
    NSArray *selectedMainRowData;
    NSArray *selectedRowData;
    
    selectedMainRowData = [yemekArray objectAtIndex:selectedSection];
    selectedRowData = [selectedMainRowData objectAtIndex:selectedRow];
    
    NSString *dishId = [NSString stringWithFormat:@"%@",[selectedRowData valueForKey:@"id"]];
    //NSLog(@"Selected DISH ID: %@", dishId);
    //NSLog(@"SEPETTEMI: %d", [self itemExists:@"dishId" withValue:dishId]);
    
    if (![self itemExists:@"dishId" withValue:dishId]) {
        //NSLog(@"NOT IN BASKET DO NOTHING");
    }
    else{
        //NSLog(@"IN BASKET Let's NOT add this.. But update the quantity");
        
        NSUInteger arrayIndex = [basketArray indexOfObjectPassingTest:^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop){
            return [[dict objectForKey:@"dishId"] isEqual:dishId];
        }];
        
        NSString *currentQuantityStr = [[basketArray objectAtIndex:arrayIndex] valueForKey:@"dishQuantitiy"];
        NSInteger currentQuantity = [currentQuantityStr integerValue];
        NSInteger newQuantity = currentQuantity - 1;
        NSString *newQuantitiyStr = [NSString stringWithFormat:@"%ld", (long)newQuantity];
        
        //NSLog(@"IN BASKET INDEX: %lu", (unsigned long)arrayIndex);
        
        NSMutableDictionary *dictionary2 = [NSMutableDictionary dictionary];
        [dictionary2 setObject:dishId forKey:@"dishId"];
        [dictionary2 setObject:newQuantitiyStr forKey:@"dishQuantitiy"];
        
        [basketArray replaceObjectAtIndex:arrayIndex withObject:dictionary2];
    }
    
    
    //NSLog(@"BASKET ARRAY: %@", basketArray);
}

- (BOOL)itemExists:(NSString *)key
         withValue:(NSString *)value {
    
    NSPredicate *exists = [NSPredicate predicateWithFormat:
                           @"%K MATCHES[c] %@", key, value];
    NSUInteger index = [basketArray indexOfObjectPassingTest:
                        ^(id obj, NSUInteger idx, BOOL *stop) {
                            return [exists evaluateWithObject:obj];
                        }];
    
    if (index == NSNotFound) {
        return NO;
    }
    
    return YES;
}

-(void)updateMainBasketCount: (int) value{
    
    mainBasketCount = mainBasketCount + value;
    
    self.basketQuantitiyLabel.text = [NSString stringWithFormat:@"%d",mainBasketCount];

}

-(void) tableView:(UITableView *) tableView willDisplayCell:(HomeCustomCell *) cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([mainMenuArray count]-1 == indexPath.row) {
        //table_page = table_page + 1;
        //[self getFeeds];
    }
    
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    NSString *name = [categoryNameArray objectAtIndex:section];
    return name;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *footer=[[UIView alloc] initWithFrame:CGRectMake(0,0,320.0,100.0)];
    footer.layer.backgroundColor = [UIColor clearColor].CGColor;
    
    
    return footer;
}


//
//
//ORDER
//
//

-(void)doOrder{
    NSLog(@"DO ORDER MAN");
    [self.waitView setHidden:false];
    
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:orderString forKey:@"order"];
    [dict setValue:qrCode forKey:@"qrcode"];
    [dict setValue:deviceLanguage forKey:@"requestLanguage"];
    [dict setValue:@"40.998" forKey:@"requestLatitude"];
    [dict setValue:@"26.998" forKey:@"requestLongitude"];
    [dict setValue:@"1" forKey:@"order_type"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:SENDORDER_API];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 //NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     //NSDictionary *hotelDetail = [dictRes valueForKey:@"hotelDetails"];
                                                     //NSDictionary *menuDetail = [dictRes valueForKey:@"menus"];
                                                     
                                                     //NSLog(@"hotelDetail: %@", hotelDetail);
                                                     //NSLog(@"menuDetail: %@", menuDetail);
                                                     //NSLog(@"menuDetail count: %lu", (unsigned long)menuDetail.count);
                                                     [self.waitIndicator setHidden:true];
                                                     [self.waitCloseButton setHidden:false];
                                                     self.pleaseWaitLabel.text = NSLocalizedString(@"Your order was sent successfully", @"Your order was sent successfully");
                                                     
                                                     
                                                     
                                                    
                                                     
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
}




-(void)checkUserActive{
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:qrCode forKey:@"qrcode"];
    [dict setValue:deviceLanguage forKey:@"requestLanguage"];
    [dict setValue:@"40.998" forKey:@"requestLatitude"];
    [dict setValue:@"26.998" forKey:@"requestLongitude"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:CHECKUSER_API];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
    
    NSLog(@"path: %@", _path);
    NSLog(@"params: %@", dict);
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 NSNumber * isUserActiveInHotel = (NSNumber *)[dictRes valueForKey:@"userActiveInHotel"];
                                                 
                                                 
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     if ([isUserActiveInHotel boolValue] == NO) {
                                                         HotelMainViewController *viewController = [[HotelMainViewController alloc] init];
                                                         [self presentViewController:viewController animated:YES completion:nil];
                                                     }
                                                     else{
                                                         [self getMenu];
                                                     }
                                                     
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                             
                                             [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"qrCode"];
                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                             
                                             
                                             HotelMainViewController *viewController = [[HotelMainViewController alloc] init];
                                             [self presentViewController:viewController animated:YES completion:nil];
                                             
                                             
                                             
                                             
                                             
                                             
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonAction:(id)sender {
    HotelMainViewController *viewController = [[HotelMainViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}
- (IBAction)waitCloseButtonAction:(id)sender {
    HotelMainViewController *viewController = [[HotelMainViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}
@end
