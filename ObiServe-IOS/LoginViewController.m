//
//  LoginViewController.m
//  ObiServe-IOS
//
//  Created by Alpaslan Bak on 27.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import "LoginViewController.h"
#import "ChoiceViewController.h"
#import "Constant.pch"
#import "AFNetworking.h"
#import "HotelMainViewController.h"
#import "UIView+MLInputDodger.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.loginButton setTitle:NSLocalizedString(@"LOG IN", @"LOG IN") forState:UIControlStateNormal];
    self.emailTextField.placeholder =NSLocalizedString(@"E-Mail", @"Email");
    self.passwordTextField.placeholder =NSLocalizedString(@"Password", @"Password");
    
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //please use the method in viewDidAppear
    self.view.shiftHeightAsDodgeViewForMLInputDodger = 50.0f;
    [self.view registerAsDodgeViewForMLInputDodger];
}

- (void)doLogin{
    
    if ([self.emailTextField.text length]==0 && [self.passwordTextField.text length]==0) {
        UIAlertView *myalert=[[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"Error!", @"Error!")
                              message:NSLocalizedString(@"Please enter your email and password!", @"Please enter your email and password!")
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
                              otherButtonTitles:nil, nil];
        [myalert show];
        return;
    }
    else if ([self.emailTextField.text length]==0) {
        UIAlertView *myalert=[[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"Error!", @"Error!")
                              message:NSLocalizedString(@"Please enter your email", @"Please enter your email")
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK",@"OK")
                              otherButtonTitles:nil, nil];
        [myalert show];
        return;
    }
    else if ([self.passwordTextField.text length]==0) {
        UIAlertView *myalert=[[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"Error!", @"Error!")
                              message:NSLocalizedString(@"Please enter your password", @"Please enter your password")                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                              otherButtonTitles:nil, nil];
        [myalert show];
        return;
    }
    
    
    [self.activityIndicatorView setHidden:false];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:self.emailTextField.text forKey:@"email"];
    [dict setValue:self.passwordTextField.text forKey:@"password"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:LOGIN_API];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             [self.activityIndicatorView setHidden:true];
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];

                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     //NSString *name=[dictRes valueForKey:@"name"];
                                                     //NSString *email=[dictRes valueForKey:@"email"];
                                                     NSString *apiKey=[dictRes valueForKey:@"apiKey"];
                                                     
                                                     [[NSUserDefaults standardUserDefaults] setObject:apiKey forKey:@"apiKey"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     
                                                     HotelMainViewController *viewController = [[HotelMainViewController alloc] init];
                                                     [self presentViewController:viewController animated:YES completion:nil];
                                                 
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             [self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                             

                                             
                                             
                                             
                                             
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
}

-(void)showAlert: (NSString *) message{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message:message
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginButtonAction:(id)sender {
    NSLog(@"LOG IN ACTION");
    [self doLogin];
}

- (IBAction)closeButton:(id)sender {
    NSLog(@"Close Click");
    ChoiceViewController *viewController = [[ChoiceViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}
@end
