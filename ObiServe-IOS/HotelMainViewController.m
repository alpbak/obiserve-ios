//
//  HotelMainViewController.m
//  ObiServe-IOS
//
//  Created by Alpaslan Bak on 27.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import "HotelMainViewController.h"
#import "QRCodeReaderViewController.h"
#import "QRCodeReader.h"
#import "AFNetworking.h"
#import "Constant.pch"
#import "SDWebImageDownloader.h"
#import "MenuListViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ServiceRequestViewController.h"
#import "HotelDetailsViewController.h"
#import "AppDelegate.h"
#import "ChoiceViewController.h"
#import <MapKit/MapKit.h>


@interface HotelMainViewController ()

@end

@implementation HotelMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    
    
    self.detailsButton.layer.cornerRadius = 20;
    self.detailsButton.clipsToBounds = YES;
    self.detailsButton.layer.borderWidth = 1;
    self.detailsButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.detailsButton.titleLabel.adjustsFontSizeToFitWidth = true;
    [self.detailsButton setTitle:NSLocalizedString(@"ACTIVITIES", @"ACTIVITIES") forState:UIControlStateNormal];
    
    self.directionsButton.layer.cornerRadius = 20;
    self.directionsButton.clipsToBounds = YES;
    self.directionsButton.layer.borderWidth = 1;
    self.directionsButton.titleLabel.adjustsFontSizeToFitWidth = true;
    self.directionsButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    [self.directionsButton setTitle:NSLocalizedString(@"FIND MY HOTEL", @"FIND MY HOTEL") forState:UIControlStateNormal];
    
    
    self.serviceRequestLabel.text = NSLocalizedString(@"I'd like to receive a service", @"I'd like to receive a service");
    self.orderRequestLabel.text = NSLocalizedString(@"I'd like to place an order", @"I'd like to place an order");
    
    NSString *pleaseScan = NSLocalizedString(@"Please scan your QR Code", @"Please scan your QR Code");
    
    NSString *langID = [[NSLocale preferredLanguages] objectAtIndex:0];
    deviceLanguage = [langID substringToIndex:2];
    NSLog(@"deviceLanguage: %@", deviceLanguage);
    
    
    
    
    UITapGestureRecognizer *serviceTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleService:)];
    UITapGestureRecognizer *orderTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleOrder:)];
    
    [self.serviceView addGestureRecognizer:serviceTap];
    [self.orderView addGestureRecognizer:orderTap];
    
    qrCode = [[NSUserDefaults standardUserDefaults]
              stringForKey:@"qrCode"];
    
    if ([qrCode length]==0) {
        [self performSelector:@selector(setupQrReader) withObject:self afterDelay:0.3 ];
    }
    else{
        [self checkUserActive];
    }
    
    
    
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [locationManager stopUpdatingLocation];
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    locationKnown = NO;
    
    
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:NSLocalizedString(@"Error", @"Error")
                               message:NSLocalizedString(@"Failed to Get Your Location. Please enable location services in order to send an order.", @"Failed to Get Your Location. Please enable location services in order to send an order")
                               delegate:nil
                               cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                               otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        //longitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        //latitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        //NSLog(@"LATITUDE: %.8f", currentLocation.coordinate.latitude);
        //NSLog(@"LONGITUDE: %.8f", currentLocation.coordinate.longitude);
        
        locationKnown = YES;
        
        currentLocationLat = currentLocation.coordinate.latitude;
        currentLocationLon = currentLocation.coordinate.longitude;
        
    }
}

- (void)handleService:(UITapGestureRecognizer *)recognizer {
    ServiceRequestViewController *viewController = [[ServiceRequestViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)handleOrder:(UITapGestureRecognizer *)recognizer {
    MenuListViewController *viewController = [[MenuListViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}

-(void)setupQrReader{
    if ([QRCodeReader supportsMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isQRAvailable"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        static QRCodeReaderViewController *vc = nil;
        static dispatch_once_t onceToken;
        
        dispatch_once(&onceToken, ^{
            QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
            vc                   = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Cancel" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:YES showTorchButton:YES];
            vc.modalPresentationStyle = UIModalPresentationFormSheet;
        });
        vc.delegate = self;
        
        [vc setCompletionWithBlock:^(NSString *resultAsString) {
            NSLog(@"Completion with result: %@", resultAsString);
        }];
        
        [self presentViewController:vc animated:YES completion:NULL];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isQRAvailable"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error")
                                                        message:NSLocalizedString(@"QR Reader not supported by the current device. Please use a device with camera functionality or allow Obiserve to use camera in your device settings", @"QR Reader not supported by the current device. Please use a device with camera functionality or allow Obiserve to use camera in your device settings")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        
        [alert show];
        
        ChoiceViewController *viewController = [[ChoiceViewController alloc] init];
        [self presentViewController:viewController animated:YES completion:nil];
    }
    
}



#pragma mark - QRCodeReader Delegate Methods

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    [self dismissViewControllerAnimated:YES completion:^{
        //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"QRCodeReader" message:result delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //[alert show];
        
        //NSLog(@"ALP ALDIK: %@", result);
        
        qrCode = result;
        
        [[NSUserDefaults standardUserDefaults] setObject:qrCode forKey:@"qrCode"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self getQrCode];
    }];
}


-(void)getQrCode{
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:qrCode forKey:@"qrcode"];
    [dict setValue:deviceLanguage forKey:@"requestLanguage"];
    [dict setValue:@"40.998" forKey:@"requestLatitude"];
    [dict setValue:@"26.998" forKey:@"requestLongitude"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:GETMENUS_API];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    //NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
    
    //NSLog(@"path: %@", _path);
    //NSLog(@"params: %@", dict);
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 //NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                     
                                                     [self performSelector:@selector(setupQrReader) withObject:self afterDelay:0.3 ];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     [self.perdeView setHidden:true];
                                                     NSDictionary *hotelDetail = [dictRes valueForKey:@"hotelDetails"];
                                                     menuDetail = [dictRes valueForKey:@"menus"];
                                                     
                                                     hotel_id = [[hotelDetail valueForKey:@"hotel_id"] objectAtIndex:0];
                                                     hotel_name = [[hotelDetail valueForKey:@"hotel_name"] objectAtIndex:0];
                                                     hotel_address = [[hotelDetail valueForKey:@"hotel_address"] objectAtIndex:0];
                                                     hotel_telephone = [[hotelDetail valueForKey:@"hotel_telephone"] objectAtIndex:0];
                                                     hotel_city = [[hotelDetail valueForKey:@"hotel_city"] objectAtIndex:0];
                                                     hotel_ilce = [[hotelDetail valueForKey:@"hotel_ilce"] objectAtIndex:0];
                                                     hotel_lat = [[hotelDetail valueForKey:@"hotel_lat"] objectAtIndex:0];
                                                     hotel_lon = [[hotelDetail valueForKey:@"hotel_lon"] objectAtIndex:0];
                                                     hotel_image = [[hotelDetail valueForKey:@"hotel_image"] objectAtIndex:0];
                                                     hotel_desc = [[hotelDetail valueForKey:@"hotel_desc"] objectAtIndex:0];
                                                     
                                                     
                                                     [[NSUserDefaults standardUserDefaults] setObject:qrCode forKey:@"qrCode"];
                                                     [[NSUserDefaults standardUserDefaults] setObject:deviceLanguage forKey:@"deviceLanguage"];
                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                     
                                                     [self loadHotelImage];
                                                     self.hotelNameLabel.text = hotel_name;
                                                     self.hotelCityLabel.text = hotel_city;
                                                     
                                                     [self getPushId];
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                             
                                             
                                             
                                             
                                             
                                             
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
}

-(void)getRoomId: (NSString *)hid{
    
    NSLog(@"GET ROOM ID:::::");
    
    
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:hid forKey:@"hid"];
    [dict setValue:deviceLanguage forKey:@"requestLanguage"];
    [dict setValue:@"40.998" forKey:@"requestLatitude"];
    [dict setValue:@"26.998" forKey:@"requestLongitude"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:GETMENUBY_ROOMID];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    /*
     NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
     NSLog(@"path: %@", _path);
     NSLog(@"params: %@", dict);
     */
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 //NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                     [self performSelector:@selector(setupQrReader) withObject:self afterDelay:0.3 ];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     [self.perdeView setHidden:true];
                                                     NSDictionary *hotelDetail = [dictRes valueForKey:@"hotelDetails"];
                                                     menuDetail = [dictRes valueForKey:@"menus"];
                                                     qrCode = [dictRes valueForKey:@"qrCode"];
                                                     
                                                     hotelLocationLat = [[dictRes valueForKey:@"hotel_lat"] floatValue];
                                                     hotelLocationLon = [[dictRes valueForKey:@"hotel_lon"] floatValue];
                                                     
                                                     hotel_id = [[hotelDetail valueForKey:@"hotel_id"] objectAtIndex:0];
                                                     hotel_name = [[hotelDetail valueForKey:@"hotel_name"] objectAtIndex:0];
                                                     hotel_address = [[hotelDetail valueForKey:@"hotel_address"] objectAtIndex:0];
                                                     hotel_telephone = [[hotelDetail valueForKey:@"hotel_telephone"] objectAtIndex:0];
                                                     hotel_city = [[hotelDetail valueForKey:@"hotel_city"] objectAtIndex:0];
                                                     hotel_ilce = [[hotelDetail valueForKey:@"hotel_ilce"] objectAtIndex:0];
                                                     hotel_lat = [[hotelDetail valueForKey:@"hotel_lat"] objectAtIndex:0];
                                                     hotel_lon = [[hotelDetail valueForKey:@"hotel_lon"] objectAtIndex:0];
                                                     hotel_image = [[hotelDetail valueForKey:@"hotel_image"] objectAtIndex:0];
                                                     hotel_desc = [[hotelDetail valueForKey:@"hotel_desc"] objectAtIndex:0];
                                                     hotelradius = [[[hotelDetail valueForKey:@"order_radius"] objectAtIndex:0] floatValue];
                                                     //hotelradius = [[hotelDetail valueForKey:@"order_radius"] floatValue];
                                                     
                                                     //NSLog(@"Radius gelen: %@", [[hotelDetail valueForKey:@"order_radius"] objectAtIndex:0]);
                                                     //NSLog(@"Radius gelen float: %f", hotelradius);
                                                     
                                                     //CHECK IF USER IS CLOSE SO THAT WE CAN CHECK IN
                                                     if (locationKnown) {
                                                         if ([self isUserCloseToOrder]) {
                                                             //user is close setup view
                                                             
                                                             [[NSUserDefaults standardUserDefaults] setObject:qrCode forKey:@"qrCode"];
                                                             [[NSUserDefaults standardUserDefaults] setObject:deviceLanguage forKey:@"deviceLanguage"];
                                                             [[NSUserDefaults standardUserDefaults] synchronize];
                                                             [self loadHotelImage];
                                                             self.hotelNameLabel.text = hotel_name;
                                                             self.hotelCityLabel.text = hotel_city;
                                                             [self getPushId];
                                                             
                                                             
                                                         }
                                                         else{
                                                             [self showNotCloseAlert];
                                                             [self performSelector:@selector(setupQrReader) withObject:self afterDelay:0.3 ];
                                                         }
                                                     }
                                                     else{
                                                         [self showLocationNotAvailableAlert];
                                                         [self performSelector:@selector(setupQrReader) withObject:self afterDelay:0.3 ];
                                                     }
                                                     
                                                     //CHECK LOCATION END
                                                     
                                                     
                                                     
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
}

-(void)checkUserActive{
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:qrCode forKey:@"qrcode"];
    [dict setValue:deviceLanguage forKey:@"requestLanguage"];
    [dict setValue:@"40.998" forKey:@"requestLatitude"];
    [dict setValue:@"26.998" forKey:@"requestLongitude"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:CHECKUSER_API];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 //NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 NSNumber * isUserActiveInHotel = (NSNumber *)[dictRes valueForKey:@"userActiveInHotel"];
                                                 
                                                 
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                     [self performSelector:@selector(setupQrReader) withObject:self afterDelay:0.3 ];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     if ([isUserActiveInHotel boolValue] == NO) {
                                                         [self setupQrReader];
                                                     }
                                                     else{
                                                         [self getQrCode];
                                                     }
                                                     
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                             
                                             
                                             
                                             
                                             
                                             
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
}

-(void)loadHotelImage{
    NSString *combined = [NSString stringWithFormat:@"%@%@", @"http://www.obiservis.com/images/", hotel_image];
    
    [self.hotelimageView sd_setImageWithURL:[NSURL URLWithString:combined]];
    
}

-(void)showAlert: (NSString *) message{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message:message
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)scannerSendClicked:(QRCodeReaderViewController *)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    NSLog(@"CLICKED %@", sender.readId);
    [self checkRoomId:sender.readId];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)detailsButtonAction:(id)sender {
    
    HotelDetailsViewController *viewControllerB = [[HotelDetailsViewController alloc] init];
    viewControllerB.htmlCode = hotel_desc;
    [self presentViewController:viewControllerB animated:YES completion:nil];
    
}

- (void)getPushId{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    //NSLog(@"PUSH USER ID: %@", appDelegate.oneSignalUserId);
    oneSignalUserId = appDelegate.oneSignalUserId;
    
    if (![oneSignalUserId isEqual: [NSNull null]] || [oneSignalUserId length] != 0) {
        [self savePushId];
    }
    
}

- (void)savePushId{
    
    //NSLog(@"savePushId 001");
    
    
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    
    //NSLog(@"savePushId 001 apikey: %@", apiKey);
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:oneSignalUserId forKey:@"gcm_id"];
    
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:@"http://www.obiservis.com/api/"]];
    NSString *_path = [NSString stringWithFormat:@"save_push_id"];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    //NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
    
    
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 //NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     //NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     //[self showAlert:messageStr];
                                                 }
                                                 else{
                                                     //[self showAlert:@"NO ERROR"];
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             //NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             //NSLog(@"ERROR: %@", error);
                                             
                                             //[self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                             
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
}

- (IBAction)directionButtonAction:(id)sender {
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // Create an MKMapItem to pass to the Maps app
        CLLocationCoordinate2D coordinate =
        CLLocationCoordinate2DMake([hotel_lat doubleValue], [hotel_lon doubleValue]);
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                       addressDictionary:nil];
        
        
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:hotel_name];
        
        // Set the directions mode to "Walking"
        // Can use MKLaunchOptionsDirectionsModeDriving instead
        NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeWalking};
        // Get the "Current User Location" MKMapItem
        MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
        // Pass the current location and destination map items to the Maps app
        // Set the direction mode in the launchOptions dictionary
        [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem]
                       launchOptions:launchOptions];
    }
}

-(BOOL)isUserCloseToOrder{
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:currentLocationLat longitude:currentLocationLon];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:hotelLocationLat longitude:hotelLocationLon];
    float distance = [location1 distanceFromLocation:location2];
    
    NSLog(@"Distance in meters: %f", distance);
    
     NSLog(@"currentLocationLat: %f", currentLocationLat);
     NSLog(@"currentLocationLon: %f", currentLocationLon);
     NSLog(@"hotelLocationLat: %f", hotelLocationLat);
     NSLog(@"hotelLocationLon: %f", hotelLocationLon);
    
    NSLog(@"Hotel radius in meters: %f", hotelradius);
    
    if (hotelradius >= distance) {
        return true;
    }
    else{
        return false;
    }
    
}

-(void)showNotCloseAlert{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message:NSLocalizedString(@"You are not close enough to the hotel. Please get closer and try again", @"You are not close enough to the hotel. Please get closer and try again")
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}

-(void)showLocationNotAvailableAlert{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message:NSLocalizedString(@"We can not get your location. Please activate location services and try again.", @"We can get your location. Please activate location services and try again.")
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}

-(void)checkRoomId: (NSString *)hid{
    
    //NSLog(@"GET ROOM ID:::::");
    
    
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:hid forKey:@"hid"];
    [dict setValue:deviceLanguage forKey:@"requestLanguage"];
    [dict setValue:@"40.998" forKey:@"requestLatitude"];
    [dict setValue:@"26.998" forKey:@"requestLongitude"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:CHECK_ROOMID];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    /*
     NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
     NSLog(@"path: %@", _path);
     NSLog(@"params: %@", dict);
     */
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                     [self performSelector:@selector(setupQrReader) withObject:self afterDelay:0.3 ];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     [self.perdeView setHidden:true];
                                                     NSDictionary *hotelDetail = [dictRes valueForKey:@"hotelDetails"];
                                                     menuDetail = [dictRes valueForKey:@"menus"];
                                                     qrCode = [dictRes valueForKey:@"qrCode"];
                                                     
                                                     hotelLocationLat = [[dictRes valueForKey:@"hotel_lat"] floatValue];
                                                     hotelLocationLon = [[dictRes valueForKey:@"hotel_lon"] floatValue];
                                                     
                                                     NSLog(@"XXX hotelLocationLat: %f", hotelLocationLat);
                                                     NSLog(@"XXX hotelLocationLon: %f", hotelLocationLon);
                                                     
                                                     
                                                     
                                                     
                                                     hotel_id = [[hotelDetail valueForKey:@"hotel_id"] objectAtIndex:0];
                                                     hotel_name = [[hotelDetail valueForKey:@"hotel_name"] objectAtIndex:0];
                                                     hotel_address = [[hotelDetail valueForKey:@"hotel_address"] objectAtIndex:0];
                                                     hotel_telephone = [[hotelDetail valueForKey:@"hotel_telephone"] objectAtIndex:0];
                                                     hotel_city = [[hotelDetail valueForKey:@"hotel_city"] objectAtIndex:0];
                                                     hotel_ilce = [[hotelDetail valueForKey:@"hotel_ilce"] objectAtIndex:0];
                                                     hotel_lat = [[hotelDetail valueForKey:@"hotel_lat"] objectAtIndex:0];
                                                     hotel_lon = [[hotelDetail valueForKey:@"hotel_lon"] objectAtIndex:0];
                                                     hotel_image = [[hotelDetail valueForKey:@"hotel_image"] objectAtIndex:0];
                                                     hotel_desc = [[hotelDetail valueForKey:@"hotel_desc"] objectAtIndex:0];
                                                     hotelradius = [[[hotelDetail valueForKey:@"order_radius"] objectAtIndex:0] floatValue];
                                                     //hotelradius = [[hotelDetail valueForKey:@"order_radius"] floatValue];
                                                     
                                                     NSLog(@"Radius gelen: %@", [[hotelDetail valueForKey:@"order_radius"] objectAtIndex:0]);
                                                     NSLog(@"Radius gelen float: %f", hotelradius);
                                                     
                                                     //CHECK IF USER IS CLOSE SO THAT WE CAN CHECK IN
                                                     if (locationKnown) {
                                                         if ([self isUserCloseToOrder]) {
                                                             //user is close setup view
                                                             [self getRoomId:hid];
                                                             
                                                             
                                                         }
                                                         else{
                                                             [self showNotCloseAlert];
                                                             [self performSelector:@selector(setupQrReader) withObject:self afterDelay:0.3 ];
                                                         }
                                                     }
                                                     else{
                                                         [self showLocationNotAvailableAlert];
                                                         [self performSelector:@selector(setupQrReader) withObject:self afterDelay:0.3 ];
                                                     }
                                                     
                                                     //CHECK LOCATION END
                                                     
                                                     
                                                     
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
}

@end
