//
//  HomeCustomCell.h
//  ObiServe
//
//  Created by Alpaslan Bak on 28.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCustomCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UIImageView *dishImage;
@property (strong, nonatomic) IBOutlet UILabel *desc;
@property (strong, nonatomic) IBOutlet UIView *butonView;
@property (strong, nonatomic) IBOutlet UIButton *plusButton;
@property (strong, nonatomic) IBOutlet UIButton *minusButton;
@property (strong, nonatomic) IBOutlet UILabel *quantitiyLabel;
@property (strong, nonatomic) IBOutlet UILabel *qtyTemp;
@property (nonatomic) int dishQuantitiy;


@end
