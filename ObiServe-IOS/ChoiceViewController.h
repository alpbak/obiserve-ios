//
//  ChoiceViewController.h
//  ObiServe-IOS
//
//  Created by Alpaslan Bak on 27.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChoiceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
- (IBAction)loginButtonAction:(UIButton *)sender;
- (IBAction)registerButtonAction:(UIButton *)sender;









@end
