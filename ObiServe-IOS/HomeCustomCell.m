//
//  HomeCustomCell.m
//  ObiServe
//
//  Created by Alpaslan Bak on 28.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import "HomeCustomCell.h"

@implementation HomeCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.butonView.layer.cornerRadius = 15;
    self.butonView.clipsToBounds = YES;
    self.butonView.layer.borderWidth = 1;
    self.butonView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.plusButton.layer.cornerRadius = 12;
    self.plusButton.clipsToBounds = YES;
    
    self.minusButton.layer.cornerRadius = 12;
    self.minusButton.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
