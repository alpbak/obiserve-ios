//
//  HotelMainViewController.h
//  ObiServe-IOS
//
//  Created by Alpaslan Bak on 27.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QRCodeReaderDelegate.h"
#import "QRCodeReaderView.h"
#import <CoreLocation/CoreLocation.h>

@interface HotelMainViewController : UIViewController<QRCodeReaderDelegate, CLLocationManagerDelegate>{
    NSString *qrCode;
    NSString *hotel_id;
    NSString *hotel_name;
    NSString *hotel_address;
    NSString *hotel_telephone;
    NSString *hotel_city;
    NSString *hotel_ilce;
    NSString *hotel_lat;
    NSString *hotel_lon;
    NSString *hotel_image;
    NSString *hotel_desc;
    NSDictionary *menuDetail;
    NSString *deviceLanguage;
    NSString *oneSignalUserId;
    CLLocationManager *locationManager;
    float currentLocationLat;
    float currentLocationLon;
    float hotelLocationLat;
    float hotelLocationLon;
    float hotelradius;
    
    BOOL locationKnown;
}
@property (strong, nonatomic) IBOutlet UIImageView *hotelimageView;
@property (strong, nonatomic) IBOutlet UIButton *detailsButton;
- (IBAction)detailsButtonAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *serviceView;
@property (strong, nonatomic) IBOutlet UIView *orderView;
@property (strong, nonatomic) IBOutlet UILabel *hotelNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *hotelCityLabel;
@property (strong, nonatomic) IBOutlet UILabel *serviceRequestLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderRequestLabel;
@property (strong, nonatomic) IBOutlet UIView *perdeView;
@property (strong, nonatomic) IBOutlet UIButton *directionsButton;
- (IBAction)directionButtonAction:(id)sender;


@end
