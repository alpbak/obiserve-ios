//
//  AppDelegate.h
//  ObiServe-IOS
//
//  Created by Alpaslan Bak on 27.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OneSignal/OneSignal.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) OneSignal *oneSignal;
@property (strong, nonatomic) NSString *oneSignalUserId;

@end

