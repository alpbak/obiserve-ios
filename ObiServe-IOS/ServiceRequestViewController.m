//
//  ServiceRequestViewController.m
//  ObiServe
//
//  Created by Alpaslan Bak on 1.06.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import "ServiceRequestViewController.h"
#import "HotelMainViewController.h"
#import "AFNetworking.h"
#import "Constant.pch"

@interface ServiceRequestViewController ()

@end

@implementation ServiceRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    
    
    self.technicView.layer.cornerRadius = 5;
    self.technicView.clipsToBounds = YES;
    
    self.wakeUpView.layer.cornerRadius = 5;
    self.wakeUpView.clipsToBounds = YES;
    
    self.houseKeepingView.layer.cornerRadius = 5;
    self.houseKeepingView.clipsToBounds = YES;
    
    self.wakeUpLabel.text = NSLocalizedString(@"Wake Up Service", @"Wake Up Service");
    self.tecnicLabel.text = NSLocalizedString(@"Technical Support", @"Technical Support");
    self.houseKeepingLabel.text = NSLocalizedString(@"Housekeeping Service", @"Housekeeping Service");
    self.requestLabel.text = NSLocalizedString(@"You can write your message", @"You can write your message");
    [self.requestSendButton setTitle:NSLocalizedString(@"Tap here to send your request", @"Tap here to send your request") forState:UIControlStateNormal];
    self.wakeUpLabel.text = NSLocalizedString(@"Wake Up Service", @"Wake Up Service");
    self.wakeUpTimeLabel.text = NSLocalizedString(@"Please select your wake up time", @"Please select your wake up time");
    
    [self.datePicker setValue:[UIColor whiteColor] forKey:@"textColor"];
    
    
    UITapGestureRecognizer *wakeupTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleWakeup:)];
    UITapGestureRecognizer *technicTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleTechnic:)];
    
    UITapGestureRecognizer *housekeepingTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleHousekeeping:)];
    
    [self.technicView addGestureRecognizer:technicTap];
    [self.wakeUpView addGestureRecognizer:wakeupTap];
    [self.houseKeepingView addGestureRecognizer:housekeepingTap];
    
    [self checkUserActive];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    locationKnown = NO;
    
    
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:NSLocalizedString(@"Error", @"Error")
                               message:NSLocalizedString(@"Failed to Get Your Location. Please enable location services in order to send an order.", @"Failed to Get Your Location. Please enable location services in order to send an order")
                               delegate:nil
                               cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                               otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        //longitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        //latitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        
        //NSLog(@"LATITUDE: %.8f", currentLocation.coordinate.latitude);
        //NSLog(@"LONGITUDE: %.8f", currentLocation.coordinate.longitude);
        
        locationKnown = YES;
        
        currentLocationLat = currentLocation.coordinate.latitude;
        currentLocationLon = currentLocation.coordinate.longitude;
        
    }
}


- (void)handleTechnic:(UITapGestureRecognizer *)recognizer {
    if (locationKnown) {
        if ([self isUserCloseToOrder]) {
            serviceCode = @"2";
            [self.requestEntryView setHidden:false];
        }
        else{
            [self showNotCloseAlert];
        }
    }
    else{
        [self showLocationNotAvailableAlert];
    }
    
    
    
    
    
    
    
    
    
}
- (void)handleHousekeeping:(UITapGestureRecognizer *)recognizer {
    if (locationKnown) {
        if ([self isUserCloseToOrder]) {
            serviceCode = @"3";
            serviceNote = @"Oda temizliği isteği";
            [self.requestEntryView setHidden:false];
        }
        else{
            [self showNotCloseAlert];
        }
    }
    else{
        [self showLocationNotAvailableAlert];
    }
    
    
    
    
    
}
- (void)handleWakeup:(UITapGestureRecognizer *)recognizer {
    if (locationKnown) {
        if ([self isUserCloseToOrder]) {
            serviceCode = @"1";
            [self.setAlarmView setHidden:false];
        }
        else{
            [self showNotCloseAlert];
        }
    }
    else{
        [self showLocationNotAvailableAlert];
    }
    
    
}

-(BOOL)isUserCloseToOrder{
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:currentLocationLat longitude:currentLocationLon];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:hotelLocationLat longitude:hotelLocationLon];
    float distance = [location1 distanceFromLocation:location2];
    
    NSLog(@"Distance in meters: %f", distance);
    
    NSLog(@"Hotel radius in meters: %f", hotelradius);
    
    if (hotelradius >= distance) {
        return true;
    }
    else{
        return false;
    }
    
}

-(void)showNotCloseAlert{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message:NSLocalizedString(@"You are not close enough to the hotel. Please get closer and try again", @"You are not close enough to the hotel. Please get closer and try again")
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}

-(void)showLocationNotAvailableAlert{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message:NSLocalizedString(@"We can not get your location. Please activate location services and try again.", @"We can get your location. Please activate location services and try again.")
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}

-(void)doServiceRequest{
    NSLog(@"DO SERVICE MAN");
    [self.requestEntryView setHidden:true];
    [self.setAlarmView setHidden:true];
    [self.waitView setHidden:false];
    
    
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    NSString *qrCode = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"qrCode"];
    
    NSString *langID = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *deviceLanguage = [langID substringToIndex:2];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:serviceCode forKey:@"requested_service"];
    [dict setValue:serviceNote forKey:@"service_note"];
    [dict setValue:qrCode forKey:@"qrcode"];
    [dict setValue:deviceLanguage forKey:@"requestLanguage"];
    [dict setValue:@"40.998" forKey:@"requestLatitude"];
    [dict setValue:@"26.998" forKey:@"requestLongitude"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:SENDSERVICE_API];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     //NSDictionary *hotelDetail = [dictRes valueForKey:@"hotelDetails"];
                                                     //NSDictionary *menuDetail = [dictRes valueForKey:@"menus"];
                                                     
                                                     //NSLog(@"hotelDetail: %@", hotelDetail);
                                                     //NSLog(@"menuDetail: %@", menuDetail);
                                                     //NSLog(@"menuDetail count: %lu", (unsigned long)menuDetail.count);
                                                     [self.waitIndicator setHidden:true];
                                                     [self.waitCloseButton setHidden:false];
                                                     self.pleaseWaitLabel.text = NSLocalizedString(@"Your service request was sent successfully", @"Your service request was sent successfully");
                                                     
                                                     
                                                     
                                                     
                                                     
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
}

-(void)showAlert: (NSString *) message{
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning!", @"Warning!")
                                                 message:message
                                                delegate:nil
                                       cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                       otherButtonTitles:nil];
    [av show];
}

- (IBAction)waitCloseButtonAction:(id)sender {
    HotelMainViewController *viewController = [[HotelMainViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)backButtonAction:(id)sender {
    HotelMainViewController *viewController = [[HotelMainViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}
- (IBAction)requestSendButton:(id)sender {
    serviceNote = [self.requestText text];
    [self.view endEditing:YES];
    [self doServiceRequest];
}
- (IBAction)closeButtonAction:(id)sender {
    [self.view endEditing:YES];
    [self.requestEntryView setHidden:true];
}
- (IBAction)alarmCloseAction:(id)sender {
    [self.view endEditing:YES];
    [self.setAlarmView setHidden:true];
}
- (IBAction)datePickerAction:(id)sender {
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"]; //24hr time format
    NSString *dateString = [outputFormatter stringFromDate:self.datePicker.date];
    NSLog(@"CHOSEN: %@", dateString);
    serviceNote = [NSString stringWithFormat:@"Saat : %@", dateString];
}

- (IBAction)alarmRequestSendButtonAction:(id)sender {
    [self doServiceRequest];
}




-(void)checkUserActive{
    NSString *deviceLanguage = [[NSUserDefaults standardUserDefaults] stringForKey:@"deviceLanguage"];
    NSString *qrCode = [[NSUserDefaults standardUserDefaults] stringForKey:@"qrCode"];
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:qrCode forKey:@"qrcode"];
    [dict setValue:deviceLanguage forKey:@"requestLanguage"];
    [dict setValue:@"40.998" forKey:@"requestLatitude"];
    [dict setValue:@"26.998" forKey:@"requestLongitude"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:CHECKUSER_API];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 //NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 NSNumber * isUserActiveInHotel = (NSNumber *)[dictRes valueForKey:@"userActiveInHotel"];
                                                 
                                                 
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     if ([isUserActiveInHotel boolValue] == NO) {
                                                         HotelMainViewController *viewController = [[HotelMainViewController alloc] init];
                                                         [self presentViewController:viewController animated:YES completion:nil];
                                                     }
                                                     else{
                                                         [self getMenu];
                                                     }
                                                     
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                             
                                             
                                             
                                             
                                             
                                             
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
}

-(void)getMenu{
    NSString *deviceLanguage = [[NSUserDefaults standardUserDefaults] stringForKey:@"deviceLanguage"];
    NSString *qrCode = [[NSUserDefaults standardUserDefaults] stringForKey:@"qrCode"];
    NSString *apiKey = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"apiKey"];
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:qrCode forKey:@"qrcode"];
    [dict setValue:deviceLanguage forKey:@"requestLanguage"];
    [dict setValue:@"40.998" forKey:@"requestLatitude"];
    [dict setValue:@"26.998" forKey:@"requestLongitude"];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc]initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    NSString *_path = [NSString stringWithFormat:GETMENUS_API];
    _path = [_path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [httpClient setDefaultHeader:@"Authorization" value:apiKey];
    
    NSLog(@"%s %@",__PRETTY_FUNCTION__,_path);
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:_path
                                                      parameters:dict];
    
    
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             
                                             if ([JSON isKindOfClass:[NSArray class]] || [JSON isKindOfClass:[NSDictionary class]]) {
                                                 NSDictionary *dictRes =JSON;
                                                 
                                                 //NSLog(@"JSON: %@", JSON);
                                                 
                                                 NSNumber * isError = (NSNumber *)[dictRes valueForKey:@"error"];
                                                 
                                                 if([isError boolValue] == YES){
                                                     //HATA
                                                     NSString *messageStr=[dictRes valueForKey:@"message"];
                                                     [self showAlert:messageStr];
                                                 }
                                                 else{
                                                     //NO HATA
                                                     NSArray *hotelDetail = [dictRes valueForKey:@"hotelDetails"];
                                                     NSArray *xxx = [hotelDetail objectAtIndex:0];
                                                     
                                                     hotelLocationLat = [[xxx valueForKey:@"hotel_lat"] floatValue];
                                                     hotelLocationLon = [[xxx valueForKey:@"hotel_lon"] floatValue];
                                                     hotelradius = [[xxx valueForKey:@"order_radius"] floatValue];
                                                     
                                                     NSLog(@"HOTEL RADIUS string: %@", [xxx valueForKey:@"order_radius"]);
                                                     NSLog(@"HOTEL RADIUS float: %f", hotelradius);
                                                     
                                                     
                                                 }
                                             }
                                             else {
                                                 
                                             }
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                             //[self.activityIndicatorView setHidden:true];
                                             
                                             NSLog(@" response %@  \n error %@ \n JSON %@",response,error,JSON);
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"ERROR: %@", error);
                                             
                                             [self showAlert:NSLocalizedString(@"Please Check your Internet Connection", @"Please Check your Internet Connection")];
                                             
                                             
                                             
                                             
                                             
                                             
                                         }];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [queue addOperation:operation];
    
    
    
}
@end
