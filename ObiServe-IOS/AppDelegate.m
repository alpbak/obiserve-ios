//
//  AppDelegate.m
//  ObiServe-IOS
//
//  Created by Alpaslan Bak on 27.05.2016.
//  Copyright © 2016 Alpaslan Bak. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "ChoiceViewController.h"
#import "RegisterViewController.h"
#import <ZHPopupView.h>


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    
    ChoiceViewController *rootViewController = [[ChoiceViewController alloc] initWithNibName:@"ChoiceViewController" bundle:nil];
    self.window.rootViewController = rootViewController;//making a view to root view
    [self.window makeKeyAndVisible];
    
    [OneSignal initWithLaunchOptions:launchOptions appId:@"0a430903-55a9-426f-bccc-c4a91797412c" handleNotificationAction:^(OSNotificationResult *result) {
        
        // This block gets called when the user reacts to a notification received
        OSNotificationPayload* payload = result.notification.payload;
        
        NSString* messageTitle = @"OneSignal Example";
        //NSString* fullMessage = [payload.body copy];
        
        if (payload.additionalData) {
            
            if(payload.title)
                messageTitle = payload.title;
            
            NSDictionary* additionalData = payload.additionalData;
            
            if (additionalData) {
                NSLog(@"additionalData: %@", additionalData);
                
                // Check for and read any custom values you added to the notification
                // This done with the "Additonal Data" section the dashbaord.
                // OR setting the 'data' field on our REST API.
                NSString* customKey = additionalData[@"segment"];
                NSString* customMessage = additionalData[@"message"];
                if (customKey){
                    NSLog(@"segment: %@", customKey);
                    NSLog(@"segment: %@", customMessage);
                }
                ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                                    iconImg:[UIImage imageNamed:@"correct_icon"]
                                                            backgroundStyle:ZHPopupViewBackgroundType_SimpleOpacity
                                                                      title:NSLocalizedString(@"Message From Hotel", @"Message From Hotel")
                                                                    content:customMessage
                                                               buttonTitles:@[NSLocalizedString(@"OK", @"OK")]
                                                        confirmBtnTextColor:nil otherBtnTextColor:nil
                                                         buttonPressedBlock:^(NSInteger btnIdx) {
                                                             
                                                             
                                                         }];
                [popupView present];
                
                
            }
        }
        

        
    }];
    
    /*
    self.oneSignal = [[OneSignal alloc]
                      initWithLaunchOptions:launchOptions
                      appId:@"0a430903-55a9-426f-bccc-c4a91797412c"
                      handleNotification:^(NSString* message, NSDictionary* additionalData, BOOL isActive) {
                          NSLog(@"OneSignal Notification opened:\nMessage: %@", message);
                          
                          if (additionalData) {
                              NSLog(@"additionalData: %@", additionalData);
                              
                              // Check for and read any custom values you added to the notification
                              // This done with the "Additonal Data" section the dashbaord.
                              // OR setting the 'data' field on our REST API.
                              NSString* customKey = additionalData[@"segment"];
                              NSString* customMessage = additionalData[@"message"];
                              if (customKey){
                                    NSLog(@"segment: %@", customKey);
                                    NSLog(@"segment: %@", customMessage);
                              }
                              ZHPopupView *popupView = [ZHPopupView popUpDialogViewInView:nil
                                                                                  iconImg:[UIImage imageNamed:@"correct_icon"]
                                                                          backgroundStyle:ZHPopupViewBackgroundType_SimpleOpacity
                                                                                    title:NSLocalizedString(@"Message From Hotel", @"Message From Hotel")
                                                                                  content:customMessage
                                                                             buttonTitles:@[NSLocalizedString(@"OK", @"OK")]
                                                                      confirmBtnTextColor:nil otherBtnTextColor:nil
                                                                       buttonPressedBlock:^(NSInteger btnIdx) {
                                                                           
                                                                           
                                                                       }];
                              [popupView present];
                              
                              
                          }
                      }];
    */
    //NSLog(@"ONESIGNAL UserId: getPushId");
    
    [OneSignal IdsAvailable:^(NSString* userId, NSString* pushToken) {
        //NSLog(@"ONESIGNAL UserId:%@", userId);
        self.oneSignalUserId = userId;
        if (pushToken != nil)
            NSLog(@"ONESIGNAL pushToken:%@", pushToken);
    }];
    
    
    /*
    ChoiceViewController *mainViewController = [[ChoiceViewController alloc] initWithNibName:@"ChoiceViewController" bundle:nil];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainViewController];
    [_window makeKeyAndVisible];
    [_window addSubview:navController.view];
     */
     
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
